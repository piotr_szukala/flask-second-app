from flask import Flask, render_template, request, json, redirect, session, url_for
from flaskext.mysql import MySQL
#from flask_mysqldb import MySQL # ten zadzialal
# Moduly do hasel
#import MySQLdb
from werkzeug.security import generate_password_hash, check_password_hash


mysql = MySQL()

app = Flask(__name__)

app.secret_key = 'why would I tell you my secret key?'


# MySQL configurations
app.config['MYSQL_DATABASE_USER'] = 'root'
app.config['MYSQL_DATABASE_PASSWORD'] = 'lucatel'
app.config['MYSQL_DATABASE_DB'] = 'BucketList'
app.config['MYSQL_DATABASE_HOST'] = 'localhost'
mysql.init_app(app)

@app.route("/")
def main():
    return render_template('index.html')

@app.route('/showSignUp')
def showSignUp():

    return render_template('signup.html')

@app.route('/signUp', methods=['POST','GET'])
def signUp():

    # create user code will be here !!
    _name = request.form['inputName']
    _email = request.form['inputEmail']
    _password = request.form['inputPassword']

    # Sprawdzanie prawidlowosci danych. Po wlaczeniu opcji wyslania formularza ajax te komunikaty pojawiaja sie tylko w konsoli.
    if _name and _email and _password:



        # Nawiazanie polaczenia z baza danych jesli wszystkie pola sa wypelnione

        conn = mysql.connect()
        cursor = conn.cursor()
        # Generowanie hasel do polaczenia
        _hashed_password = generate_password_hash(_password)
        # Uruchomienie procedury sp_createUser, ktra byla zdefiniowana w SQL (sprawdza czy dany user juz jest w bazie danych). Jesli ok to commit
        # cursor.callproc('sp_createUser',(_name,_email,_hashed_password))

        query = "INSERT INTO `tbl_user`(`user_email`, `user_name`, `user_password`) VALUES ('%s','%s','%s')" % (_email, _name, _password)
        cursor.execute(query)
        # dodanie wszystkich wynikow z zapytania query cursor
        data = cursor.fetchall()

        # Pojawienie sie komunikatow dla wyslania danych
        if len(data) is 0:
            conn.commit()
            return json.dumps({'message':'User created successfully !'})
        else:
            return json.dumps({'error':str(data[0])})

        return json.dumps({'html':'<span>All fields good !!</span>'})
    else:
        return json.dumps({'html':'<span>Enter the required fields</span>'})


@app.route('/showSignin')
def showSignin():
    return render_template('signin.html')

@app.route('/validateLogin', methods=['POST'])
def signIn():
    try:
        _name = request.form['inputName']
        _password = request.form['inputPassword']


        # Polaczenie z baza danych mysql
        conn = mysql.connect() #bylo connect()
        cursor = conn.cursor()
        query = "SELECT * from tbl_user where user_name = '%s' and user_password = '%s'" % (_name, _password)
        cursor.execute(query)
        data = cursor.fetchall()

        if _name and _password:
            if str(data[0][3]) == _password:
                user_name = data[0][2]
                session['user'] = data[0][0]
                return redirect(url_for('userHome', user_name = user_name))
            else:
                return render_template('error.html',error = 'Wrong Email address or Password')
        else:
            return render_template('error.html',error = 'Prosze wprawdzdzic login i haslo')

    except Exception as e:
        return render_template('error.html',error = str(e))

    finally:
        cursor.close()
        conn.close()

@app.route('/userHome/<user_name>')
def userHome(user_name):
    if session.get('user'):
        return render_template('userHome.html')
    else:
        return render_template('error.html',error = 'Unauthorized Access')


@app.route('/logout')
def logout():
    session.pop('user',None)
    return redirect('/')



if __name__ == '__main__':
    app.run()

