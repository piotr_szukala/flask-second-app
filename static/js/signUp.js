$(function() {
    $('#btnSignUp').click(function() {

        $.ajax({
            url: '/signUp', //wysyła formularz na ta sama strone
            data: $('form').serialize(), //form to element html a metoda serialize() zmienia dane z firmularza w tekst do wysylki
            type: 'POST',
            success: function(response) {
                console.log(response);
            },
            error: function(error) {
                console.log(error);
            }
        });
    });
});
